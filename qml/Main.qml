import QtQuick 2.7
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'gotify.bhdouglass'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    property QtObject colors: QtObject {
        readonly property color divider: UbuntuColors.porcelain
        readonly property color text: UbuntuColors.porcelain
        readonly property color background: '#3f51b5'
    }

    Settings {
        id: settings
        property string url
        property string appToken
        property string clientToken
    }

    Client {
        id: client
    }

    PageStack {
        id: pageStack

        Component.onCompleted: push(Qt.resolvedUrl('MessagePage.qml'))
    }
}
