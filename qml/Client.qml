import QtQuick 2.7

Item {
    function getUrl(path, appToken) {
        var url = settings.url;
        if (settings.url.indexOf('http://') == -1 && settings.url.indexOf('https://') == -1) {
            url = 'http://' + settings.url;
        }

        if (url[url.length - 1] != '/') {
            url += '/';
        }

        url += path + '?token=';
        url += appToken ? settings.appToken : settings.clientToken;
        return url;
    }

    function sendRequest(method, path, body, appToken, callback) {
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                // TODO catch json errors
                var data = JSON.parse(xhr.responseText);
                if (xhr.status == 200) {
                    callback(null, data);
                }
                else {
                    console.log('error', xhr.status);
                    var error = 'Unknown error';
                    if (data && data.errorDescription) {
                        error = data.errorDescription;
                    }

                    callback(error, null);
                }
            }
        };

        xhr.open(method, getUrl(path, appToken), true);
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');

        if (body) {
            xhr.send(JSON.stringify(body));
        }
        else {
            xhr.send();
        }
    }

    function sendMessage(title, message, callback) {
        sendRequest('POST', 'message', { title: title, message: message }, true, callback);
    }

    function getAllMessages(callback) {
        sendRequest('GET', 'message', null, false, callback);
    }

    function getVersion(callback) {
        sendRequest('GET', 'version', null, true, callback);
    }
}
