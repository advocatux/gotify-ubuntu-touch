import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Ubuntu.Components.Popups 1.3

Page {
    id: messagePage
    header: PageHeader {
        id: header
        title: i18n.tr('Gotify')

        StyleHints {
            foregroundColor: colors.text
            backgroundColor: colors.background
            dividerColor: colors.divider
        }

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                },

                Action {
                    text: i18n.tr('Settings')
                    iconName: 'settings'

                    onTriggered: pageStack.push(Qt.resolvedUrl('SettingsPage.qml'))
                },

                Action {
                    text: i18n.tr('Messages')
                    iconName: 'view-list-symbolic'

                    onTriggered: pageStack.push(Qt.resolvedUrl('MessageListPage.qml'));
                }
            ]
        }
    }

    function shouldSend() {
        if (title.text && message.text) {
            client.sendMessage(title.text, message.text, function(err) {
                if (err !== null) {
                    PopupUtils.open(dialogComponent, messagePage, {
                        error: err
                    });
                }
                else {
                    PopupUtils.open(dialogComponent, messagePage);
                    message.text = '';
                    title.text = '';
                }
            });
        }
    }

    Flickable {
        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        contentHeight: column.height + units.gu(4)
        clip: true

        ColumnLayout {
            id: column
            anchors {
                top: parent.top
                right: parent.right
                left: parent.left
                rightMargin: units.gu(1)
                leftMargin: units.gu(1)
                topMargin: units.gu(2)
            }

            spacing: units.gu(2)

            Label {
                text: i18n.tr('Title')
                textSize: Label.Large
            }

            TextField {
                id: title
                onAccepted: shouldSend()
                Layout.fillWidth: true
            }

            Label {
                text: i18n.tr('Message')
                textSize: Label.Large
            }

            TextField {
                id: message
                onAccepted: shouldSend()
                Layout.fillWidth: true
            }

            Button {
                text: i18n.tr('Send')
                color: UbuntuColors.green

                onClicked: shouldSend()
            }

            Label {
                visible: !settings.url || !settings.appToken
                text: i18n.tr('Configure server and token in the settings page')
                color: UbuntuColors.red
            }
        }
    }

    Component {
        id: dialogComponent

        Dialog {
            id: dialog

            property string error: ''

            title: error ? i18n.tr('Error') : i18n.tr('Success')
            text: error ? error : i18n.tr('Your message was sent')

            Button {
                text: i18n.tr('Ok')
                color: UbuntuColors.green
                onClicked: PopupUtils.close(dialog)
            }
        }
    }
}
