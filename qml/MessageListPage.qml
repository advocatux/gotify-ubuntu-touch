import QtQuick 2.7
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3

Page {
    property var messages: []
    property bool loading: false
    property string error: ''

    Component.onCompleted: {
        if (settings.url && settings.clientToken) {
            loading = true;
            client.getAllMessages(function(err, data) {
                loading = false;

                if (err) {
                    error = err;
                    messages = [];
                }
                else {
                    error = '';
                    messages = data.messages;
                }
            });
        }
    }

    header: PageHeader {
        id: header
        title: i18n.tr('Messages')

        StyleHints {
            foregroundColor: colors.text
            backgroundColor: colors.background
            dividerColor: colors.divider
        }

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr('About')
                    iconName: 'info'

                    onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'));
                },

                Action {
                    text: i18n.tr('Settings')
                    iconName: 'settings'

                    onTriggered: pageStack.push(Qt.resolvedUrl('SettingsPage.qml'))
                }
            ]
        }
    }

    ActivityIndicator {
        visible: loading
        running: loading
        anchors.centerIn: parent
    }

    Label {
        anchors.centerIn: parent
        width: parent.width
        visible: error

        text: error
        color: UbuntuColors.red

        wrapMode: Label.WordWrap
        horizontalAlignment: Label.AlignHCenter
    }

    Label {
        anchors.centerIn: parent
        visible: !settings.url || !settings.clientToken

        text: i18n.tr('Configure server and token in the settings page')
        color: UbuntuColors.red

        horizontalAlignment: Label.AlignHCenter
    }

    Flickable {
        visible: !loading

        anchors {
            top: header.bottom
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }
        clip: true

        // TODO pull to refresh

        ListView {
            anchors.fill: parent
            model: messages

            delegate: ListItem {
                height: layout.height

                // TODO option to delete message
                ListItemLayout {
                    id: layout
                    title.text: modelData.title
                    subtitle.text: modelData.message
                }
            }
        }
    }
}
