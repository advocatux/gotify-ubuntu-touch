# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gotify.bhdouglass package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gotify.bhdouglass\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-30 04:13+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/AboutPage.qml:9 ../qml/MessageListPage.qml:41
#: ../qml/SettingsPage.qml:22 ../qml/MessagePage.qml:21
msgid "About"
msgstr ""

#: ../qml/AboutPage.qml:64 ../qml/MessagePage.qml:10 gotify.desktop.in.h:1
msgid "Gotify"
msgstr ""

#: ../qml/AboutPage.qml:71
msgid "Unofficial Gotify client app for Ubuntu Touch"
msgstr ""

#: ../qml/AboutPage.qml:79
msgid ""
"A Brian Douglass app, consider donating if you like it and want to see more "
"apps like it!"
msgstr ""

#: ../qml/AboutPage.qml:87
msgid "Donate"
msgstr ""

#: ../qml/MessageListPage.qml:30 ../qml/MessagePage.qml:35
msgid "Messages"
msgstr ""

#: ../qml/MessageListPage.qml:48 ../qml/SettingsPage.qml:11
#: ../qml/MessagePage.qml:28
msgid "Settings"
msgstr ""

#: ../qml/MessageListPage.qml:79 ../qml/MessagePage.qml:115
msgid "Configure server and token in the settings page"
msgstr ""

#: ../qml/SettingsPage.qml:55
msgid "Gotify Server URL"
msgstr ""

#: ../qml/SettingsPage.qml:69
msgid "App Token"
msgstr ""

#: ../qml/SettingsPage.qml:83
msgid "App token for sending messages"
msgstr ""

#: ../qml/SettingsPage.qml:87
msgid "Client Token"
msgstr ""

#: ../qml/SettingsPage.qml:101
msgid "Client token for reading messages"
msgstr ""

#: ../qml/SettingsPage.qml:105
msgid "Verify url and tokens"
msgstr ""

#: ../qml/SettingsPage.qml:131
msgid "Found Gotify version:"
msgstr ""

#: ../qml/MessagePage.qml:85
msgid "Title"
msgstr ""

#: ../qml/MessagePage.qml:96
msgid "Message"
msgstr ""

#: ../qml/MessagePage.qml:107
msgid "Send"
msgstr ""

#: ../qml/MessagePage.qml:129
msgid "Error"
msgstr ""

#: ../qml/MessagePage.qml:129
msgid "Success"
msgstr ""

#: ../qml/MessagePage.qml:130
msgid "Your message was sent"
msgstr ""

#: ../qml/MessagePage.qml:133
msgid "Ok"
msgstr ""
